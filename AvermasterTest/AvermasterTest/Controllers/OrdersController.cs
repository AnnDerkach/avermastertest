﻿using AvermasterTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AvermasterTest.Controllers
{
    public class OrdersController: Controller
    {
        private DataOrderRepositoty repo = new DataOrderRepositoty();


        [HttpGet]
        public ActionResult Сatalog()
        {
            return View(repo.GetOrderList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Products = new SelectList(repo.GetProductsList()).SelectedValues;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Phone,CustomerName,CustomerLastname,ProductId,DeliveryType,DeliveryPointId")] Order order)
        {
            if (ModelState.IsValid)
            {
                order.CreateDate = DateTime.Now.Date;      
                repo.Create(order);
                return RedirectToAction("Сatalog", "Orders");
            }

            return View(order);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            Order order = repo.GetOrder(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Phone,CustomerName,CustomerLastname,ProductId,DeliveryType,DeliveryPointId")] Order order)
        {
            if (ModelState.IsValid)
            {
                order.EditDate = DateTime.Now;
                repo.Update(order);
                return RedirectToAction("Сatalog", "Orders");
            }
            return View(order);
        }

        public ActionResult Delete(int id)
        {
            Order order = repo.GetOrder(id);
            return View(order);
        }

  
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = repo.GetOrder(id);
            repo.Delete(order.Id);
            return RedirectToAction("Сatalog", "Orders");
        }
    }
}