namespace AvermasterTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CreateDate", c => c.DateTime());
            AddColumn("dbo.Orders", "EditDate", c => c.DateTime());
            AddColumn("dbo.Orders", "Price", c => c.Decimal());

        }
        
        public override void Down()
        {
           
        }
    }
}
