﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvermasterTest.Models
{
    public class DeliveryPoint
    {
        public int Id { get; set; }

        public string Address { get; set; }
    }
}