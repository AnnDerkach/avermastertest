﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AvermasterTest.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Дата создания")]
        [Column(TypeName = "DateTime2")]
        public DateTime? CreateDate { get; set; }

        [Column(TypeName = "DateTime2")]
        [Display(Name = "Дата обновления")]
        public DateTime? EditDate { get; set; }
        public Decimal? Price { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "Контактный номер")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string CustomerName { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string CustomerLastname { get; set; }

        [Required]
        [Display(Name = "Товар")]
        public int ProductId { get; set; }

        [Required]
        [Display(Name = "Способ дроставки")]
        public string DeliveryType { get; set; }

        [Required]
        [Display(Name = "Точка выдачи")]
        public int DeliveryPointId { get; set; }
    }
}