﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AvermasterTest.Models
{
    public class DbModel: DbContext
    {
        static DbModel() {
            Database.SetInitializer<DbModel>(null);
        }

        public DbModel() : base("DefaultConnection") {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<DeliveryPoint> DeliveryPoint { get; set; }

        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<Product>().Property(x => x.Price).HasPrecision(18, 2);
        }
    }
}