﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvermasterTest.Models
{


    public class DataOrderRepositoty
    {
        private DbModel db;

        public DataOrderRepositoty()
        {
            this.db = new DbModel();
        }

        public IEnumerable<Order> GetOrderList()
        {
            return db.Orders;
        }
        public IEnumerable<SelectListItem> GetProductsList()
        {
            IEnumerable<SelectListItem> myCollection = db.Products
                                           .Select(i => new SelectListItem()
                                           {
                                               Value = i.Id.ToString()
                                           });
            return myCollection;
        }


        public Order GetOrder(int id)
        {
            return db.Orders.Find(id);
        }

        public void Create(Order order)
        {
            try
            {
                db.Orders.Add(order);
                db.SaveChanges();
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.InnerException);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

        }

        public void Update(Order order)
        {
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Order order = db.Orders.Find(id);
            if (order != null)
                db.Orders.Remove(order);
            db.SaveChanges();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}